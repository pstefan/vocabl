import QtQuick 2.0
import QtQuick.Window 2.1
import "."


Rectangle {
    id: gv_

    property int dpi: Screen.pixelDensity*25.4
    signal showScreen(string msg)

    width: Screen.width
    height: Screen.height
    color: "black"

    property bool menu_shown: false

    /* this contains the "menu" */
    MyMenu {
        onChangeScreen: {
            gv_.showScreen(msg)
            gv_.menu_shown = true
            gv_.onMenu()
        }
    }

    /* this rectangle contains the "normal" view in your app */
    Rectangle {
        id: normal_view_
        anchors.fill: parent
        color: "white"

        /* this is what moves the normal view aside */
        transform: Translate {
            id: game_translate_
            x: 0
            Behavior on x { NumberAnimation { duration: 400; easing.type: Easing.OutQuad } }
        }

        /* this is the menu shadow */
        BorderImage {
            id: menu_shadow_
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.margins: -10
            z: -1 /* this will place it below normal_view_ */
            visible: gv_.menu_shown
            source: "shadow.png"
            border { left: 12; top: 12; right: 12; bottom: 12 }
        }

        TopBar {
            id: top_bar
            Component.onCompleted: setHeading("O aplikaci")
            z: 10
        }


        /* this is normal contant*/
        Flickable {
            width: parent.width
            height: parent.height - top_bar.height
            anchors.topMargin: 0.1 * dpi
            anchors.top: top_bar.bottom
            contentWidth: parent.width
            contentHeight: r.height
            Rectangle {
                id: r
                width: parent.width
                height: text_heading.height + text_content.height + text_content2.height + source.height + text_contribute.height + sign.height + 2 * dpi
                Column {
                    id: text_heading
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: top_bar.bottom
                    anchors.topMargin: 0.1 * dpi
                    Text {
                        text: "VocabL"
                        font.bold: true
                        font.pointSize: 0.1 * dpi
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                    Text {
                        text: "zkoušení slovíček"
                        font.pointSize: 0.1 * dpi
                    }
                }
                Text {
                    id: text_content
                    text: "Tato aplikace vznikla jako výukový projekt na vyzkoušení spojení platformy Android, frameworku Qt a C++. Tím je trošku negativně ovlivněna velikost aplikace, protože v ní musí být přibaleny grafické Qt knihovny."
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.Wrap
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: text_heading.bottom
                    anchors.topMargin: 0.2 * dpi
                    font.pointSize: 0.05 * dpi
                    width: parent.width * 0.85
                }
                Text {
                    id: text_content2
                    text: "Slovníky jsou ve formátu CSV s oddělovačem ',' a musí mít příponu '.csv'. Výchozí adresář pro načítání slovníků je /sdcard/VocabL/. Tento adresář lze změnit v nastavení. Ukázkové slovníky lze získat ve složce dictionaries/ v mém repozitáři (odkaz níže)."
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.Wrap
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: text_content.bottom
                    anchors.topMargin: 0.2 * dpi
                    font.pointSize: 0.05 * dpi
                    width: parent.width * 0.85
                }
                Column {
                    id: source
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: text_content2.bottom
                    anchors.topMargin: 0.2 * dpi
                    Text {
                        text: "Zdrojové soubory: "
                        font.pointSize: 0.05 * dpi
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                    Text {
                        text: "<html><a href='https://bitbucket.org/pstefan/vocabl'>https://bitbucket.org/pstefan/vocabl</a></html>"
                        onLinkActivated: Qt.openUrlExternally(link)
                        font.pointSize: 0.05 * dpi
                    }
                }
                Text {
                    id: text_contribute
                    text: "Pokud se budete chtít podílet na vývoji této aplikace, budu rád, když si uděláte fork výše uvedeného repozitáře a pošlete mi pull request s opravami chyb či novými funkcemi. Pokud chcete jen nahlásit chybu, použijte prosím Issues v mém repozitáři. Budu se je snažit rychle vyřídit."
                    anchors.horizontalCenter: parent.horizontalCenter
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.Wrap
                    anchors.top: source.bottom
                    anchors.topMargin: 0.2 * dpi
                    font.pointSize: 0.05 * dpi
                    width: parent.width * 0.85
                }
                Row {
                    id: sign
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: text_contribute.bottom
                    anchors.topMargin: 0.2 * dpi
                    Text {
                        text: "Petr Stefan  -  "
                        font.bold: true
                        font.pointSize: 0.05 * dpi
                    }
                    Text {
                        text: "<html><a href='mailto:ptr.stef@gmail.com'>ptr.stef@gmail.com</a></html>"
                        font.pointSize: 0.05 * dpi
                        onLinkActivated: Qt.openUrlExternally(link)
                    }
                }
            }
        }

        /* put this last to "steal" touch on the normal window when menu is shown */
        MouseArea {
            anchors.fill: parent
            enabled: gv_.menu_shown
            onClicked: gv_.onMenu();
        }
    }

    /* this functions toggles the menu and starts the animation */
    function onMenu()
    {
        game_translate_.x = gv_.menu_shown ? 0 : gv_.width * 0.9
        gv_.menu_shown = !gv_.menu_shown;
    }
}
