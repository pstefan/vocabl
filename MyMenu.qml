import QtQuick 2.0
import QtQuick.Window 2.0
import "."

Rectangle {
    id: menu_view_
    property int dpi: Screen.pixelDensity*25.4
    signal changeScreen(string msg)
    anchors.fill: parent
    color: "#303030";
    opacity: gv_.menu_shown ? 1 : 0
    Behavior on opacity {
        NumberAnimation { duration: 300 }
    }

    Rectangle {
        id: app_name
        height: 0.3 * dpi
        width: parent.width
        color: "transparent"
        Text {
            text: "VocabL"
            font.pointSize: 0.1 * dpi
            font.bold: true
            color: "white"
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: parent.width * 0.9 / 2 - width / 2
        }
    }

    /* this is my sample menu content (TODO: replace with your own) */
    Component {
        id: listDelegate
        Rectangle {
            id: menu_item_
            function actionClicked(index) {
                switch(listModel.get(index).internalIndex) {
                case 1:
                    menu_view_.changeScreen("Zkouseni.qml")
                    break;
                case 2:
                    menu_view_.changeScreen("About.qml")
                    break;
                case 3:
                    menu_view_.changeScreen("Settings.qml")
                    break;
                default:
                    break;
                }
            }

            height: 0.4 * dpi;
            width: parent.width
            color: "transparent"
            Text {
                anchors {
                    left: parent.left
                    leftMargin: 0.2 * dpi
                    verticalCenter: parent.verticalCenter
                }
                text: name
                color: "white"
                font.pointSize: 0.08 * dpi
            }
            Rectangle {
                height: 2
                width: parent.width - 0.1 * parent.width - 0.2 * dpi
                color: "gray"
                anchors {
                    bottom: parent.bottom
                    left: parent.left
                    leftMargin: 0.1 * dpi
                }
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    actionClicked(index)
                }
                hoverEnabled: false
                onPressed: { parent.color = "#606060" }
                onReleased: { parent.color = "transparent" }
                onExited: { parent.color = "transparent" }
                onCanceled: { parent.color = "transparent" }
            }
        }
    }

    ListModel {
             id: listModel
             ListElement {
                 name: "Zkoušení"
                 internalIndex: 1
             }
             ListElement {
                 name: "Nastavení"
                 internalIndex: 3
             }
             ListElement {
                 name: "O aplikaci"
                 internalIndex: 2
             }
    }
    ListView {
        width: parent.width
        height: parent.height
        anchors { margins: 0.1 * dpi}
        anchors.top: app_name.bottom
        model: listModel
        delegate: listDelegate
        clip: true
    }
}
