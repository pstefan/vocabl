import QtQuick 2.3
import QtQuick.Window 2.1
import QtQuick.Controls 1.2
import "."


Rectangle {
    id: gv_

    property int dpi: Screen.pixelDensity*25.4
    signal showScreen(string msg)

    width: Screen.width
    height: Screen.height
    color: "black"

    property bool menu_shown: false

    /* this contains the "menu" */
    MyMenu {
        onChangeScreen: {
            gv_.showScreen(msg)
            gv_.menu_shown = true
            gv_.onMenu()
        }
    }

    /* this rectangle contains the "normal" view in your app */
    Rectangle {
        id: normal_view_
        anchors.fill: parent
        color: "white"

        MouseArea {
            anchors.fill: parent
        }

        /* this is what moves the normal view aside */
        transform: Translate {
            id: game_translate_
            x: 0
            Behavior on x { NumberAnimation { duration: 400; easing.type: Easing.OutQuad } }
        }

        /* this is the menu shadow */
        BorderImage {
            id: menu_shadow_
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.margins: -10
            z: -1 /* this will place it below normal_view_ */
            visible: gv_.menu_shown
            source: "shadow.png"
            border { left: 12; top: 12; right: 12; bottom: 12 }
        }

        /* menu button with top bar */
        TopBar {
            id: top_bar
            Component.onCompleted: setHeading("Zkoušení")
        }

        Row {
            id: score
            anchors.right: parent.right
            anchors.top: top_bar.bottom
            //width: textp.width > textn.width ? textp.width + 0.06 * dpi : textn.width + 0.06 * dpi
            //height: 0.4 * dpi
            //color: "transparent"
            Rectangle {
                id: positive
                color: "lightgreen"
                width: textp.width + 0.06 * dpi
                height: 0.2 * dpi
                radius: 10
                border.color: "green"
                border.width: 2
                Text {
                    id: textp
                    font.bold: true
                    font.pointSize: 0.08 * dpi
                    anchors.centerIn: parent
                    text: "0"
                }
            }
            Rectangle {
                id: negative
                color: "#FFC3CE"
                width: textn.width + 0.06 * dpi
                height: 0.2 * dpi
                //anchors.left: positive.right
                radius: 10
                border.color: "red"
                border.width: 2
                Text {
                    id: textn
                    font.bold: true
                    font.pointSize: 0.08 * dpi
                    anchors.centerIn: parent
                    text: "0"
                }
            }
            Component.onCompleted: {
                textp.text = testing_vocabulary.getPositiveScore()
                textn.text = testing_vocabulary.getNegativeScore()
            }
        }

        /* this is page content */
        Rectangle {
            id: rectangle1
            anchors.horizontalCenter: parent.horizontalCenter
            //anchors.verticalCenter: parent.verticalCenter
            anchors.top: top_bar.bottom
            anchors.topMargin: 0.3 * dpi
            width: parent.width
            height: parent.height / 2
            color: "transparent"
            Rectangle {
                id: text_label
                height: text_label_text.height
                width: parent.width * 0.75
                anchors.horizontalCenter: parent.horizontalCenter
                color: "transparent"
                Text {
                    id: text_label_text
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.bottom
                    text: ""
                    font.pointSize: 0.1 * dpi
                    Component.onCompleted: text_label_text.text = testing_vocabulary.getCurrent()
                    width: parent.width
                    wrapMode: Text.Wrap
                    horizontalAlignment: Text.AlignHCenter
                }
            }
            Rectangle {
                id: last_ok
                width: last_ok_text.width + 0.1 * dpi
                height: 0.2 * dpi
                color: "lightgreen"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: text_label.bottom
                radius: 30
                anchors.topMargin: parent.height / 12 - height / 2
                visible: false
                Text {
                    id: last_ok_text
                    text: "OK"
                    anchors.centerIn: parent
                    color: "green"
                    font.pointSize: 0.05 * dpi
                }
            }
            Rectangle {
                id: last_fail
                width: last_fail_text.width + 0.1 * dpi
                height: 0.2 * dpi
                color: "#ffc2cd"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: text_label.bottom
                radius: 30
                anchors.topMargin: parent.height / 12 - height / 2
                visible: false
                Text {
                    id: last_fail_text
                    text: "Špatně"
                    anchors.centerIn: parent
                    color: "red"
                    font.pointSize: 0.05 * dpi
                }
            }
            TextField {
                id: text_input
                font.pointSize: 0.08 * dpi
                text: ""
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: text_label.bottom
                anchors.topMargin: parent.height / 6
                width: parent.width * 0.75
                height: 0.25 * dpi
                //focus: true
                onAccepted: {
                    parent.click()

                }
                //Component.onCompleted: { forceActiveFocus(5) }
            }
            Text {
                id: right_answer
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: text_input.bottom
                anchors.topMargin: parent.height / 12 - height / 2
                color: "red"
                font.pointSize: 0.05 * dpi
                text: ""
                wrapMode: Text.Wrap
                horizontalAlignment: Text.AlignHCenter
            }

            Rectangle {
                id: submit_button
                radius: 10
                border.width: 2
                border.color: "gray"
                width: parent.width * 0.75
                height: 0.25 * dpi
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: text_input.bottom
                anchors.topMargin: parent.height / 6
                gradient: Gradient {
                     GradientStop { id:stop1; position: 0.0; color: "#D4D4D4" }
                     GradientStop { id:stop2; position: 1.0; color: "#303030" }
                }
                Text {
                    text: "ZKONTROLUJ!"
                    anchors.centerIn: parent
                    font.pointSize: 0.08 * dpi
                    color: "white"
                }
                MouseArea {
                    anchors.fill: parent
                    onPressed: {
                        stop1.color = "#303030"
                        stop2.color = "#D4D4D4"
                    }
                    onExited: {
                        stop1.color = "#D4D4D4"
                        stop2.color = "#303030"
                    }
                    onClicked: {
                        parent.parent.click()
                        text_input.focus
                    }
                }
            }
            function click() {
                text_label_text.text = testing_vocabulary.getNext(text_input.text.toString())
                text_input.text = ""
                textp.text = testing_vocabulary.getPositiveScore()
                textn.text = testing_vocabulary.getNegativeScore()
                last_ok.visible = testing_vocabulary.wasLastOK()
                last_fail.visible = !testing_vocabulary.wasLastOK()
                right_answer.text = testing_vocabulary.getLastAnswer()
                Qt.inputMethod.show()
            }
        }


        /* put this last to "steal" touch on the normal window when menu is shown */
        MouseArea {
            anchors.fill: parent
            enabled: gv_.menu_shown
            onClicked: gv_.onMenu();
        }
    }

    /* this functions toggles the menu and starts the animation */
    function onMenu()
    {
        game_translate_.x = gv_.menu_shown ? 0 : gv_.width * 0.9
        gv_.menu_shown = !gv_.menu_shown;
    }
}
