#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQuick/QQuickView>
#include <QQmlContext>
#include <dirent.h>
#include <string>
#include <vector>
#include <regex>

#include "testing_vocabulary.h"
#include "config_master.h"

int getdir(std::string dir, QStringList& files)
{
    DIR *dp;
    struct dirent *dirp;
    if((dp  = opendir(dir.c_str())) == NULL) {
        return -1;
    }

    while ((dirp = readdir(dp)) != NULL) {
        std::string file = dirp->d_name;
        if(std::regex_match(file, std::regex(".*\\.csv"))) {
            files.push_back(file.c_str());
        }
    }
    closedir(dp);
    return 0;
}

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);


    QQuickView view;
    testing_vocabulary my_testing;
    config_master my_config;
    my_testing.setFile(my_config.get_ext_path() + my_config.get_voc_name());
    QStringList dataList;
    getdir(my_config.get_ext_path().toStdString(), dataList);

    view.rootContext()->setContextProperty("testing_vocabulary", &my_testing);
    view.rootContext()->setContextProperty("config_master", &my_config);
    view.rootContext()->setContextProperty("fileModel", QVariant::fromValue(dataList));
    view.setSource(QUrl(QStringLiteral("qrc:///main.qml")));
    view.show();


    return app.exec();
}
