#include "vocabulary_master.h"
#include <fstream>
#include <chrono>

vocabulary_master::vocabulary_master() : generator_(std::chrono::high_resolution_clock::now().time_since_epoch().count()),
    distr_(nullptr)
{
}
vocabulary_master::~vocabulary_master()
{
    delete distr_;
}

bool vocabulary_master::load(std::string file)
{
    data_.clear();
    delete distr_;
    distr_ = nullptr;
    std::ifstream istr(file);
    if(istr.is_open()) {
        std::string line;
        while(std::getline(istr, line)) {
            if(!line.empty()) {
                if(line[0] == '#')
                    continue;
                size_t pos = line.find_first_of(',');
                if(pos == std::string::npos) {
                    data_.clear();
                    return false;
                }
                std::string first = line.substr(0, pos);
                std::string second = line.substr(pos + 1);
                data_.push_back(mypair(first.c_str(), second.c_str()));
            }
        }
        istr.close();
        distr_ = new std::uniform_int_distribution<size_t>(0, data_.size() - 1);
        return true;
    }
    else {
        return false;
    }
}

mypair vocabulary_master::get_random()
{
    if(data_.empty()) {
        return mypair("Nejprve načti slovník!","");
    }
    else {
        return data_[(*distr_)(generator_)];
    }
}
