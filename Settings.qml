import QtQuick 2.2
import QtQuick.Window 2.1
import QtQuick.Dialogs 1.2
import QtQuick.Controls 1.1
import "."


Rectangle {
    id: gv_

    signal showScreen(string msg)

    width: Screen.width
    height: Screen.height
    color: "black"

    property bool menu_shown: false
    property int dpi: Screen.pixelDensity*25.4

    /* this contains the "menu" */
    MyMenu {
        onChangeScreen: {
            gv_.showScreen(msg)
            gv_.menu_shown = true
            gv_.onMenu()
        }
    }

    /* this rectangle contains the "normal" view in your app */
    Rectangle {
        id: normal_view_
        anchors.fill: parent
        color: "white"
        MouseArea {
            anchors.fill: parent
        }

        /* this is what moves the normal view aside */
        transform: Translate {
            id: game_translate_
            x: 0
            Behavior on x { NumberAnimation { duration: 400; easing.type: Easing.OutQuad } }
        }

        /* this is the menu shadow */
        BorderImage {
            id: menu_shadow_
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.margins: -10
            z: -1 /* this will place it below normal_view_ */
            visible: gv_.menu_shown
            source: "shadow.png"
            border { left: 12; top: 12; right: 12; bottom: 12 }
        }

        /* quick and dirty menu "button" for this demo (TODO: replace with your own) */
        TopBar {
            id: top_bar
            Component.onCompleted: setHeading("Nastavení")
        }


        /* this is my settings contant*/
        Rectangle {
            id: reset_ok
            width: reset_ok_text.width + 0.1 * dpi
            height: 0.2 * dpi
            color: "lightgreen"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: reset_button.top
            radius: 30
            anchors.bottomMargin: parent.height / 12 - height / 2
            visible: false
            Text {
                id: reset_ok_text
                text: "OK"
                anchors.centerIn: parent
                color: "green"
                font.pointSize: 0.06 * dpi
            }
        }
        Rectangle {
            id: reset_button
            radius: 10
            border.width: 2
            border.color: "gray"
            width: parent.width * 0.75
            height: 0.25 * dpi
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: top_bar.bottom
            anchors.topMargin: parent.height / 6
            gradient: Gradient {
                 GradientStop { id:stop1; position: 0.0; color: "#D4D4D4" }
                 GradientStop { id:stop2; position: 1.0; color: "#303030" }
            }
            Text {
                text: "Resetuj skóre"
                anchors.centerIn: parent
                font.pointSize: 0.08 * dpi
                color: "white"
            }
            MouseArea {
                anchors.fill: parent
                onPressed: {
                    stop1.color = "#303030"
                    stop2.color = "#D4D4D4"
                }
                onExited: {
                    stop1.color = "#D4D4D4"
                    stop2.color = "#303030"
                }
                onClicked: {
                    testing_vocabulary.resetScore()
                    reset_ok.visible = true
                }
            }
        }
        Text {
            id: display_file
            text: config_master.get_ext_path() + config_master.get_voc_name()
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: file_button.top
            anchors.bottomMargin: parent.height / 12 - height / 2
            font.pointSize: 0.08 * dpi
            MouseArea {
                anchors.fill: parent
            }
        }

        Rectangle {
            id: file_button
            radius: 10
            border.width: 2
            border.color: "gray"
            width: parent.width * 0.75
            height: 0.25 * dpi
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: reset_button.bottom
            anchors.topMargin: parent.height / 6
            gradient: Gradient {
                 GradientStop { id:stop3; position: 0.0; color: "#d4d4d4" }
                 GradientStop { id:stop4; position: 1.0; color: "#303030" }
            }
            Text {
                text: "Vyber soubor"
                anchors.centerIn: parent
                font.pointSize: 0.08 * dpi
                color: "white"
            }
            MouseArea {
                anchors.fill: parent
                onPressed: {
                    stop3.color = "#303030"
                    stop4.color = "#D4D4D4"
                }
                onExited: {
                    stop3.color = "#D4D4D4"
                    stop4.color = "#303030"
                }
                onClicked: {
                    file_select.visible = true
                }
            }
        }

        Rectangle {
            id: file_select
            anchors.fill: parent
            color: "#aa000000"
            visible: false
            z: 10
            MouseArea {
                anchors.fill: parent
            }
            Rectangle {
                id: file_select_inner
                anchors.centerIn: parent
                color: "white"
                opacity: 1
                width: parent.width * 0.9
                height: parent.height * 0.75
                ListView {
                     anchors.fill: parent
                     anchors.bottomMargin: 0.3 * dpi
                     model: fileModel
                     delegate: file_select_delegate
                     clip: true
                }
                Rectangle {
                    width: parent.width
                    height: 0.3 * dpi
                    anchors.bottom: parent.bottom
                    Rectangle {
                        width: parent.width
                        height: 2
                        color: "#0066CC"
                        anchors.top: parent.top
                    }
                    Text {
                        text: "Zrušit"
                        font.pointSize: 0.08 * dpi
                        anchors.centerIn: parent
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: file_select.visible = false
                        onPressed: { parent.color = "#C0C0C0" }
                        onReleased: { parent.color = "transparent" }
                        onExited: { parent.color = "transparent" }
                        onCanceled: { parent.color = "transparent" }
                    }
                }
            }
        }
        Component {
            id: file_select_delegate
            Rectangle {
                width: file_select_inner.width
                height: 0.3 * dpi
                Text {
                    //width: file_select.width
                    font.pointSize: 0.08 * dpi
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: modelData
                }
                Rectangle {
                    width: parent.width * 0.8
                    height: 2
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.bottom
                    color: "grey"
                }
                MouseArea {
                    anchors.fill: parent
                    onPressed: { parent.color = "#C0C0C0" }
                    onReleased: { parent.color = "transparent" }
                    onExited: { parent.color = "transparent" }
                    onCanceled: { parent.color = "transparent" }
                    onClicked: {
                        file_select.visible = false
                        config_master.set_voc_name(modelData)
                        testing_vocabulary.setFile(config_master.get_ext_path() + config_master.get_voc_name())
                        display_file.text = config_master.get_ext_path() + config_master.get_voc_name()
                    }
                }
            }
        }

        Text {
            id: display_path
            text: config_master.get_ext_path()
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: path_button.top
            anchors.bottomMargin: parent.height / 12 - height / 2
            font.pointSize: 0.08 * dpi
            MouseArea {
                anchors.fill: parent
            }
        }
        Rectangle {
            id: path_button
            radius: 10
            border.width: 2
            border.color: "gray"
            width: parent.width * 0.75
            height: 0.25 * dpi
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: file_button.bottom
            anchors.topMargin: parent.height / 6
            gradient: Gradient {
                 GradientStop { id:stop5; position: 0.0; color: "#d4d4d4" }
                 GradientStop { id:stop6; position: 1.0; color: "#303030" }
            }
            Text {
                text: "Uprav cestu"
                anchors.centerIn: parent
                font.pointSize: 0.08 * dpi
                color: "white"
            }
            MouseArea {
                anchors.fill: parent
                onPressed: {
                    stop5.color = "#303030"
                    stop6.color = "#D4D4D4"
                }
                onExited: {
                    stop5.color = "#D4D4D4"
                    stop6.color = "#303030"
                }
                onClicked: {
                    path_select.visible = true
                }
            }
        }
        Rectangle {
            id: path_select
            anchors.fill: parent
            color: "#aa000000"
            visible: false
            z: 10
            MouseArea {
                anchors.fill: parent
            }
            Rectangle {
                id: path_select_inner
                width: parent.width * 0.9
                height: 1 * dpi
                color: "white"
                anchors.centerIn: parent
                TextField {
                    id: path_input
                    font.pointSize: 0.08 * dpi
                    text: config_master.get_ext_path()
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: path_select_inner.top
                    anchors.topMargin: 0.2 * dpi
                    width: parent.width * 0.9
                    height: 0.25 * dpi
                }
                Rectangle {
                    height: 0.25 * dpi
                    width: parent.width
                    anchors.bottom: path_select_inner.bottom
                    anchors.horizontalCenter: path_select_inner.horizontalCenter
                    Rectangle {
                        id: path1
                        width: parent.width / 2
                        height: 0.3 * dpi
                        anchors.bottom: parent.bottom
                        anchors.left: parent.left
                        Rectangle {
                            width: parent.width
                            height: 2
                            color: "#0066CC"
                            anchors.top: parent.top
                            anchors.horizontalCenter: parent.horizontalCenter
                        }
                        Text {
                            text: "Zrušit"
                            font.pointSize: 0.08 * dpi
                            anchors.centerIn: parent
                        }
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                path_select.visible = false
                                path_input.text = config_master.get_ext_path()
                            }
                            onPressed: { parent.color = "#C0C0C0" }
                            onReleased: { parent.color = "transparent" }
                            onExited: { parent.color = "transparent" }
                            onCanceled: { parent.color = "transparent" }
                        }
                    }
                    Rectangle {
                        id: path3
                        width: parent.width / 2
                        height: 0.3 * dpi
                        anchors.bottom: parent.bottom
                        anchors.right: parent.right
                        Rectangle {
                            width: parent.width
                            height: 2
                            color: "#0066CC"
                            anchors.top: parent.top
                            anchors.horizontalCenter: parent.horizontalCenter
                        }
                        Text {
                            text: "OK"
                            font.pointSize: 0.08 * dpi
                            anchors.centerIn: parent
                        }
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                path_select.visible = false
                                config_master.set_ext_path(path_input.text)
                                display_path.text = path_input.text
                                path_info.visible = true
                            }
                            onPressed: { parent.color = "#C0C0C0" }
                            onReleased: { parent.color = "transparent" }
                            onExited: { parent.color = "transparent" }
                            onCanceled: { parent.color = "transparent" }
                        }
                    }
                    Rectangle {
                        id: path2
                        width: 2
                        height: 0.3 * dpi
                        color: "#0066CC"
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.bottom: parent.bottom
                    }
                }
            }
        }
        Rectangle {
            id: path_info
            anchors.fill: parent
            color: "#aa000000"
            visible: false
            z: 10
            MouseArea {
                anchors.fill: parent
            }
            Rectangle {
                color: "white"
                anchors.centerIn: parent
                width: parent.width * 0.8
                height: path_info_text.height + 0.5 * dpi
                Text {
                    id: path_info_text
                    font.pointSize: 0.08 * dpi
                    text: "Změny se projeví po restartování aplikace."
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.Wrap
                    anchors.top: parent.top
                    anchors.topMargin: 0.1 * dpi
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width * 0.9
                }
                Rectangle {
                    id: path_info_ok
                    height: 0.3 * dpi
                    width: parent.width
                    anchors.top: path_info_text.bottom
                    anchors.topMargin: 0.1 * dpi
                    anchors.bottom: parent.bottom
                    Rectangle {
                        width: parent.width
                        height: 2
                        color: "#0066CC"
                    }
                    Text {
                        font.pointSize: 0.08 * dpi
                        text: "OK"
                        anchors.centerIn: parent
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: path_info.visible = false
                        onPressed: { parent.color = "#C0C0C0" }
                        onReleased: { parent.color = "transparent" }
                        onExited: { parent.color = "transparent" }
                        onCanceled: { parent.color = "transparent" }
                    }
                }
            }
        }

        /* put this last to "steal" touch on the normal window when menu is shown */
        MouseArea {
            anchors.fill: parent
            enabled: gv_.menu_shown
            onClicked: gv_.onMenu();
        }
    }

    /* this functions toggles the menu and starts the animation */
    function onMenu()
    {
        game_translate_.x = gv_.menu_shown ? 0 : gv_.width * 0.9
        gv_.menu_shown = !gv_.menu_shown;
    }
}
