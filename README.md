# VocabL - Vocabulary Learning App
## Introduction

I wrote this app to practise my programming skills and learn something new. It's my first app for Android platform and first app that is using QtQuick UI (qml syntax). Because of using multiplatform Qt libraries, it's possible to run this app on Linux desktop computer too.

## Build instructions

**Prerequisites:**

* QtCreator
* Android SDK
* Android NDK
* JDK (Java)
* Qt libraries for Android

For QtCreator setup you can follow [official documentation](http://qt-project.org/doc/qtcreator-3.0/creator-developing-android.html).

When you have properly configured QtCreator, just open project in it (*VocabL.pro* file), select build target Android and run Build. Your .apk file will be located in *Release/bin* directory.

## Install instructions

* Move your compiled .apk file to your Android device, open it in some file explorer and click on it to install. Then follow instructions on the screen.
**Note:** You need to have apps from unknown sources (Settings > Security > Unknown sources) enabled!

* You need to have some dictionaries. You can download my samples from *dictionaries/* directory in this repo or you can create your own ones. A dictionary is standard CSV file with ',' as a delimitier. Lines starting with '#' are comments. All your dictionaries must have .csv extension and should be in *VocabL/* directory in your external storage - */sdcard/VocabL/* by default. This path can be changed in Settings page in the app.

## Contributions

If you want to contribute to this project, feel free to fork and send me a pull request :-).

## License

This project is under GNU General Public License v3. For more details please read LICENSE file or [http://www.gnu.org/licenses/gpl-3.0.txt](http://www.gnu.org/licenses/gpl-3.0.txt).

## Changelog

* 1.0 - Initial release.
* 1.1 - Fixed UI items size across various screens size with various density.
        **Known bugs:** Press "ZKONTROLUJ!" button without focus in TextField cause some UI resolution corruption. Hide and reopen keyboard solve this issue.