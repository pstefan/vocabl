#ifndef TESTING_VOCABULARY_H
#define TESTING_VOCABULARY_H

#include <QString>
#include <QObject>
#include "vocabulary_master.h"

class testing_vocabulary : public QObject
{
    Q_OBJECT
public:
    testing_vocabulary();
    Q_INVOKABLE QString getNext(QString input);
    Q_INVOKABLE QString getCurrent();
    Q_INVOKABLE QString getPositiveScore();
    Q_INVOKABLE QString getNegativeScore();
    Q_INVOKABLE bool wasLastOK();
    Q_INVOKABLE QString getLastAnswer();
    Q_INVOKABLE void resetScore();
    Q_INVOKABLE void setFile(QString file);
private:
    size_t positive_score_;
    size_t negative_score_;
    QString current_task_;
    QString current_answer_;
    QString last_answer_print_;
    QString file_;
    bool last_ok_;
    vocabulary_master my_vocab_;
};

#endif // TESTING_VOCABULARY_H
