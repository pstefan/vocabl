#ifndef CONFIG_MASTER_H
#define CONFIG_MASTER_H

#include <QString>
#include <QObject>

class config_master : public QObject
{
    Q_OBJECT
public:
    config_master();
    ~config_master() {}
    Q_INVOKABLE void read_conf();
    Q_INVOKABLE QString get_ext_path();
    Q_INVOKABLE QString get_voc_name();
    Q_INVOKABLE void set_ext_path(QString path);
    Q_INVOKABLE void set_voc_name(QString name);
private:
    bool directory_exists_(QString path);
    void create_config_();
    QString config_dir_;
    QString config_file_name_;
    QString current_ext_path_;
    QString current_voc_name_;
};

#endif // CONFIG_MASTER_H
