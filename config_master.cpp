#include "config_master.h"
#include <string>
#include <dirent.h>
#include <fstream>
#include <cstdio>

config_master::config_master() : config_dir_("/data/data/org.qtproject.vocabl/"), config_file_name_("config.txt"),
    current_ext_path_("/sdcard/VocabL/"), current_voc_name_("")
{
    read_conf();
}

void config_master::read_conf()
{
    if(directory_exists_(config_dir_)) {
        std::string conf_file_string = config_dir_.toStdString() + config_file_name_.toStdString();
        std::ifstream conf_file(conf_file_string.c_str());
        if (conf_file.is_open()) {
            std::string str;
            std::string f1 = "extpath=";
            std::string f2 = "vocabulary=";
            size_t pos;
            while(std::getline(conf_file, str)) {
                pos = str.find(f1);
                if(pos == 0 && pos != std::string::npos) {
                    current_ext_path_ = str.substr(f1.length()).c_str();
                }
                pos = str.find(f2);
                if(pos == 0 && pos != std::string::npos) {
                    current_voc_name_ = str.substr(f2.length()).c_str();
                }
            }
            conf_file.close();
        }
        else {
            create_config_();
        }
    }
}

QString config_master::get_ext_path()
{
    return current_ext_path_;
}

QString config_master::get_voc_name()
{
    return current_voc_name_;
}

void config_master::set_ext_path(QString path)
{
    current_ext_path_ = path;
    config_file_name_ = "config.txt.temp";
    create_config_();
    config_file_name_ = "config.txt";
    std::string onestr = config_dir_.toStdString() + "config.txt";
    std::string twostr = config_dir_.toStdString() + "config.txt.temp";
    remove(onestr.c_str());
    rename(twostr.c_str(), onestr.c_str());
}

void config_master::set_voc_name(QString name)
{
    current_voc_name_ = name;
    config_file_name_ = "config.txt.temp";
    create_config_();
    config_file_name_ = "config.txt";
    std::string onestr = config_dir_.toStdString() + "config.txt";
    std::string twostr = config_dir_.toStdString() + "config.txt.temp";
    remove(onestr.c_str());
    rename(twostr.c_str(), onestr.c_str());
}

bool config_master::directory_exists_(QString path)
{
    std::string spath = path.toStdString();
    if (spath.empty())
        return false;
    DIR *pDir;
    bool bExists = false;
    pDir = opendir(spath.c_str());
    if (pDir != NULL) {
        bExists = true;
        (void) closedir (pDir);
    }
    return bExists;
}

void config_master::create_config_()
{
    std::string conf_file_string = config_dir_.toStdString() + config_file_name_.toStdString();
    std::ofstream config_file(conf_file_string.c_str());
    if(config_file.is_open()) {
        config_file << "extpath=" << current_ext_path_.toStdString() << std::endl;
        config_file << "vocabulary=" << current_voc_name_.toStdString() << std::endl;
        config_file.close();
    }
}
