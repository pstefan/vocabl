import QtQuick 2.2
import QtQuick.Window 2.1

import "."


Rectangle {
    width: Screen.width
    height: Screen.height

    Loader {
       id: loader
       source: "Zkouseni.qml"
    }

    Connections {
       target: loader.item
       onShowScreen: loader.source = msg;
    }
}
