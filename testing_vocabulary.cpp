#include "testing_vocabulary.h"

testing_vocabulary::testing_vocabulary() : positive_score_(0), negative_score_(0), last_ok_(true)
{
}

QString testing_vocabulary::getNext(QString input) {
    if(input == current_answer_) {
        last_ok_ = true;
        positive_score_++;
        last_answer_print_.clear();
    }
    else {
        last_ok_ = false;
        negative_score_++;
        last_answer_print_ = current_task_ + " - " + current_answer_;
    }
    auto word = my_vocab_.get_random();
    current_task_ = word.first;
    current_answer_ = word.second;
    return current_task_;
}

QString testing_vocabulary::getCurrent()
{
    return current_task_;
}

QString testing_vocabulary::getPositiveScore()
{
    return QString::number(positive_score_);
}

QString testing_vocabulary::getNegativeScore()
{
    return QString::number(negative_score_);
}

bool testing_vocabulary::wasLastOK()
{
    return last_ok_;
}

QString testing_vocabulary::getLastAnswer()
{
    return last_answer_print_;
}

void testing_vocabulary::resetScore()
{
    positive_score_ = 0;
    negative_score_ = 0;
}

void testing_vocabulary::setFile(QString file)
{
    file_ = file;
    my_vocab_.load(file.toStdString());
    auto word = my_vocab_.get_random();
    current_task_ = word.first;
    current_answer_ = word.second;
}


