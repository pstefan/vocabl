#ifndef VOCABULARY_MASTER_H
#define VOCABULARY_MASTER_H

#include <QString>
#include <string>
#include <vector>
#include <random>

typedef std::pair<QString, QString> mypair;

class vocabulary_master
{
public:
    vocabulary_master();
    ~vocabulary_master();
    bool load(std::string file);
    mypair get_random();
private:
    std::vector<mypair> data_;
    std::default_random_engine generator_;
    std::uniform_int_distribution<size_t>* distr_;
};

#endif // VOCABULARY_MASTER_H
