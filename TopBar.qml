import QtQuick 2.2
import QtQuick.Window 2.1

Rectangle {
    property int dpi: Screen.pixelDensity*25.4
    id: menu_bar_
    anchors.top: parent.top
    width: parent.width;
    height: 0.3 * dpi;
    color: "#0066CC"
    signal setHeading(string text)
    MouseArea {
        anchors.fill: parent
    }
    Rectangle {
        id: menu_button_
        width: parent.height * 0.75
        height: parent.height
        color: "#006acc"
        anchors {left: parent.left; verticalCenter: parent.verticalCenter; margins: 24 }
        Column {
            anchors {left: parent.left; verticalCenter: parent.verticalCenter;}
            width: parent.width
            spacing: parent.height / 9
                Rectangle {
                    width: parent.width;
                    height: parent.parent.height / 9
                    color: "white";
                }
                Rectangle {
                    width: parent.width;
                    height: parent.parent.height / 9
                    color: "white";
                }
                Rectangle {
                    width: parent.width;
                    height: parent.parent.height / 9
                    color: "white";
                }
        }
        MouseArea { id: ma_; anchors.fill: parent; onClicked: {gv_.onMenu(); Qt.inputMethod.hide();} }
    }
    Text {
        id: heading_text
        text: ""
        color: "white"
        font.pointSize: 0.1 * dpi
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
    }

    onSetHeading: heading_text.text = text
}
